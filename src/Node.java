import java.net.InetSocketAddress;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

/**
 * Node class and methods
 *
 * @author Benjamin Deleze, Cedric Maire, Mathilde Raynal
 */

@SuppressWarnings("JavaDoc")
class Node {
    // The IPv4 address of the node
    private final InetSocketAddress IP;
    // The unique ID of the node
    private final int ID;
    // A map with the different messages of this node (identified by their content) as key and a set of nodes that
    // have received the message as element
    private final HashMap<Integer, HashSet<Integer>> ackMap;
    // Is the node correct or faulty ?
    // private final boolean correct;
    // The sequence number of the last message from this node that was delivered
    private int lastMessage;

// --Commented out by Inspection START (26.10.19, 17:20):
//    /**
//     * An extensive constructor for Node, used only for testing purposes.
//     *
//     * @param ip
//     * @param correct
//     * @param id
//     * @param seq
//     * @param map
//     */
//    public Node(InetSocketAddress ip, boolean correct, int id, int seq, HashMap<Integer, HashSet<Integer>> map) {
//        IP = ip;
//        this.correct = correct;
//        ID = id;
//        lastMessage = seq;
//        ackMap = new HashMap<>(map);
//    }
// --Commented out by Inspection STOP (26.10.19, 17:20)

    /**
     * The constructor that is normally used
     *
     * @param ip
     * @param id
     */
    Node(InetSocketAddress ip, int id) {
        IP = ip;
        // this.correct = true;
        ID = id;
        lastMessage = 0;
        ackMap = new HashMap<>();
    }

    /**
     * A method to get the node associated with the given id
     *
     * @param nodes, the list of nodes
     * @param id, the id of the node we want
     * @return the corresponding node
     */
    static Node getNodeWithID(List<Node> nodes, int id) {
        for (Node n : nodes) {
            if (n.getId() == id) {
                return n;
            }
        }
        return null;
    }

    /**
     * A getter for the IP
     *
     * @return the ip
     */
    InetSocketAddress getIP() {
        return IP;
    }

    /**
     * A getter for the id
     *
     * @return the id
     */
    int getId() {
        return ID;
    }

    /**
     * A getter for the last message sequence number
     *
     * @return the number of the last message delivered
     */
    int getLastMessage() {
        return lastMessage;
    }

    /**
     * Increase by one the value of lastMessage
     */
    void updateLastMessage() {
        ++lastMessage;
    }

    /**
     * Update the ackMap to say that srcID received our message content
     *
     * @param content, the content of the message
     * @param srcID,   the id of the process the ack is from
     */
    void addAck(int content, int srcID) {
        HashSet<Integer> s;
        if ((s = ackMap.get(content)) == null) {
            s = new HashSet<>();
        }
        s.add(srcID);
        ackMap.put(content, s);
    }

    /**
     * Return the number of acks the node received for a given message
     *
     * @param content, the content of the message
     * @return the number of acks or -1 if the message is not in the map
     */
    int numberOfAck(int content) {
        int result;
        if (ackMap.get(content) == null) {
            result = -1;
        } else {
            result = ackMap.get(content).size();
        }
        return result;
    }

    /**
     * Checks if a given node has acked a certain message from us
     *
     * @param from,    the id of the node we want to check we have an ack from
     * @param content, the content of the message
     * @return true if the node with id from has acked our message content
     */
    boolean hasAckFrom(int from, int content) {
        HashSet<Integer> s;
        if ((s = ackMap.get(content)) == null) {
            return false;
        }
        return s.contains(from);
    }

    @Override
    public boolean equals(Object o) {
        if (!(o instanceof Node))
            return false;
        Node no = (Node) o;
        return this.ID == no.getId() && this.IP.equals(no.getIP());
    }

    @Override
    public String toString() {
        return "(" +
                IP.toString() +
                "," +
                ID +
                "," +
                lastMessage +
                "," +
                ackMap.toString() +
                ")";
    }
}
