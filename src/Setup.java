import java.net.InetSocketAddress;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.HashMap;

class Setup {
    private static int id;
    private static String membershipFilePath;
    private static int numberMessagesToSend;
    private static HashMap<Integer, Node> nodes;
    private static List<Thread> threadsToKill;
    private static Set<Integer> dependencies;

    static void setup(String[] args) {
        parse(args);
        // print();
        threadsToKill = new ArrayList<>();
    }

    /**
     * The creation and initialization of a new signal handler
     *
     * @param session, the current session
     * @return a new SignalHandler
     */
    static SignalHandler setupSignalHandler(Session session) {
        SignalHandler signalHandler = new SignalHandler(session);
        signalHandler.start();

        return signalHandler;
    }

    /**
     * Parse the given arguments
     *
     * @param args, the arguments we want to parse
     */
    private static void parse(String[] args) {
        if (args.length < 2) {
            System.out.println("ERROR: please provide and id and a file containing the setup parameters.");
            System.exit(1);
        }

        id = Integer.parseInt(args[0]);
        membershipFilePath = args[1];
        numberMessagesToSend = Integer.parseInt(args[2]);
        nodes = new HashMap<>();
        dependencies = new HashSet();

        boolean foundSelf = false;

        try {
            List<String> lines = Files.readAllLines(Paths.get(membershipFilePath));
            int numberNodes = Integer.parseInt(lines.get(0));

            for (int i = 1; i <= numberNodes; ++i) {
                String[] parts = lines.get(i).split("\\s+");

                int nodeID = Integer.parseInt(parts[0]);
                String nodeIP = parts[1];
                if (nodeID == id) {
                    if (foundSelf) {
                        throw new Exception("found self twice in membership file");
                    } else {
                        foundSelf = true;
                    }
                }
                int nodePort = Integer.parseInt(parts[2]);

                InetSocketAddress nodeAddress = new InetSocketAddress(nodeIP, nodePort);
                nodes.put(nodeID, new Node(nodeAddress, nodeID));
            }

            for (int i = numberNodes + 1; i < lines.size(); ++i) {
                String[] parts = lines.get(i).split("\\s+");

                if (Integer.parseInt(parts[0]) == id) {
                    for (int j = 1; j < parts.length; ++j) {
                        dependencies.add(Integer.parseInt(parts[j]));
                    }
                }
            }

            if (!foundSelf) {
                throw new Exception("didn't find self in membership file");
            }
        } catch (Exception exception) {
            exception.printStackTrace();
            System.exit(1);
        }
    }

    /**
     * A getter for the id
     *
     * @returnm the id
     */
    static int getID() {
        return id;
    }

    /**
     * A getter for the nodes
     *
     * @return, the list of nodes
     */
    static HashMap<Integer, Node> getNodes() {
        return nodes;
    }

    /**
     * Print the setup parameters
     */
    private static void print() {
        System.out.println("##### Setup Parsed Parameters #####");
        System.out.println("ID: " + id);
        System.out.println("MembershipFilePath: " + membershipFilePath);
        System.out.println("Nodes: " + nodes.toString());
        System.out.println("Dependencies: " + dependencies.toString());
        System.out.println("###################################");
    }

    /**
     * A getter for the existing threads
     *
     * @return the list of existing threads
     */
    static List<Thread> getThreadsToKill() {
        return threadsToKill;
    }

    /**
     * A getter for the number of message to broadcast
     *
     * @return the number of message to broadcast
     */
    static int getNumberMessagesToBroadcast() {
        return numberMessagesToSend;
    }

    /**
     * Add the given thread to the list of existing threads
     *
     * @param thread, the thread we want to add
     */
    static void addThread(Thread thread) {
        threadsToKill.add(thread);
    }

    public static Set<Integer> getDependencies() {
        return dependencies;
    }
}
