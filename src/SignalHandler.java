import sun.misc.Signal;

class SignalHandler extends Thread {
    private static final Signal signalUSR2 = new Signal("USR2");
    private static final Signal signalTERM = new Signal("TERM");
    private static final Signal signalINT = new Signal("INT");

    SignalHandler(Session session) {
        Signal.handle(signalUSR2, new SigHandler(/*this, */session));
        Signal.handle(signalTERM, new SigHandler(/*this, */session));
        Signal.handle(signalINT, new SigHandler(/*this, */session));
    }

    static class SigHandler implements sun.misc.SignalHandler {
        // --Commented out by Inspection (26.10.19, 17:20):final SignalHandler process;
        private final Session session;

        private SigHandler(/* SignalHandler process, */Session session) {
            super();
            // this.process = process;
            this.session = session;
        }

        @Override
        public void handle(Signal signal) {
            if (signal.equals(SignalHandler.signalUSR2)) {
                this.session.start();
            } else if (signal.equals(SignalHandler.signalTERM) || signal.equals(SignalHandler.signalINT)) {
                this.session.stop();
            }
        }
    }
}
