import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;

class Logger {
  private static final ConcurrentLinkedQueue<Pair> deliveries = new ConcurrentLinkedQueue<>();
  private static final ConcurrentHashMap<Pair, Integer> mapForsetB = new ConcurrentHashMap<>();
  private static final Set<Pair> broadcasts = mapForsetB.keySet(0);


  /**
   * Add a new message to the broadcast's log
   *
   * @param message, the message that we want to log
   */
  static void broadcast(Message message) {
    if (message.getCreatorID() == Setup.getID()) {
      broadcasts.add(new Pair(System.nanoTime(), "b " + message.getContent()));
    }
  }

  /**
   * Add a new message to the delivery's log
   *
   * @param message, the message that we want to log
   */
  static void delivery(Message message) {
    deliveries.add(new Pair(System.nanoTime(), "d " + message.getCreatorID() + " " + message.getContent()));
  }

  /**
   * Write the content of broadcasts and deliveries in a file
   */
  static void write() {
    List<Pair> logs = new ArrayList<>(broadcasts);
    logs.addAll(deliveries);

    Comparator<Pair> pairComparator = Comparator.comparing(Pair::getKey).thenComparing(Pair::getValue, (s1, s2) -> {
      if (s1.length() < s2.length()) {
        return -1;
      } else if (s1.length() > s2.length()) {
        return 1;
      } else {
        return s1.compareTo(s2);
      }
    });

    logs.sort(pairComparator);

    List<String> logStrings = new ArrayList<>();
    logs.forEach(pair -> logStrings.add(pair.getValue()));

    String logString = String.join("\n", logStrings);

    File outputFile = new File("da_proc_" + Setup.getID() + ".out");
    try {
      //noinspection ResultOfMethodCallIgnored
      outputFile.createNewFile();
    } catch (IOException e) {
      e.printStackTrace();
      System.out.println("ERROR: file creation failed");
    }

    try (PrintWriter out = new PrintWriter(outputFile)) {
      out.println(logString);
      out.flush();
    } catch (FileNotFoundException e) {
      e.printStackTrace();
      System.out.println("ERROR: file write failed");
    }
  }

  /**
   * A getter for broadcasts
   *
   * @return broadcasts
   */
  static Set<Pair> getBroadcasts() {
    return broadcasts;
  }

  /**
   * A getter for deliveries
   *
   * @return deliveries
   */
  static ConcurrentLinkedQueue<Pair> getDeliveries() {
    return deliveries;
  }
}

/**
 * A helper class representing a Pair
 */
class Pair {
  private final Long key;
  private final String value;

  Pair(Long l, String s) {
    key = l;
    value = s;
  }

  Long getKey() {
    return key;
  }

  String getValue() {
    return value;
  }

  @Override
  public String toString() {
    return "(" + key + ", " + value + ")";
  }

  @Override
  public boolean equals(Object o) {
      if (!(o instanceof Pair))
          return false;
      Pair no = (Pair) o;
      return this.value.equals(no.value);
  }
  @Override
  public int hashCode() {
      return value.hashCode();
  }
}
