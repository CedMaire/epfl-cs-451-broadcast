public class Da_proc {
    public static void main(String[] args) {
        Setup.setup(args);

        Session session = new Session(Setup.getNodes(), Setup.getID());
        SignalHandler signalHandler = Setup.setupSignalHandler(session);

        while (!signalHandler.isInterrupted()) {
            try {
                Thread.sleep(10000);
            } catch (Exception exception) {
                System.out.println("ERROR MAIN: " + exception.toString());
            }
        }
    }
}
