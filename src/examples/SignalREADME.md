## Compile and execute
```bash
javac MainTmp.java
java -cp . MainTmp
```

## Send signals
```bash
kill -s USR2 <pid>
kill -s INT <pid>
kill -s TERM <pid>
```
