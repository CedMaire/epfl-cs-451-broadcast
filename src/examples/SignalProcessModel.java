package examples;

import sun.misc.Signal;
import sun.misc.SignalHandler;

class SignalProcessModel extends Thread {
    public SignalProcessModel() {
        SigHandlerUsr2 sigHandlerUsr2 = new SigHandlerUsr2(this);
        SigHandlerInt sigHandlerTerm = new SigHandlerInt(this);
        SigHandlerTerm sigHandlerInt = new SigHandlerTerm(this);

        Signal signalTerm = new Signal("TERM");
        Signal signalInt = new Signal("INT");
        Signal signalUsr2 = new Signal("USR2");

        Signal.handle(signalInt, sigHandlerInt);
        Signal.handle(signalTerm, sigHandlerTerm);
        Signal.handle(signalUsr2, sigHandlerUsr2);

        this.start();
    }

    public void run() {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                Thread.sleep(100);
            } catch (Exception e) {
                // exception
            }
        }
    }

    public static class SigHandlerUsr2 implements SignalHandler {
        final SignalProcessModel p;

        private SigHandlerUsr2(SignalProcessModel p) {
            super();
            this.p = p;
        }

        @Override
        public void handle(Signal signal) {
            System.out.format("Handling signal: %s\n", signal.toString());
        }
    }

    public static class SigHandlerTerm implements SignalHandler {
        final SignalProcessModel p;

        private SigHandlerTerm(SignalProcessModel p) {
            super();
            this.p = p;
        }

        @Override
        public void handle(Signal signal) {
            System.out.format("Handling signal: %s\n", signal.toString());
            p.interrupt();
        }
    }

    public static class SigHandlerInt implements SignalHandler {
        final SignalProcessModel p;

        private SigHandlerInt(SignalProcessModel p) {
            super();
            this.p = p;
        }

        @Override
        public void handle(Signal signal) {
            System.out.format("Handling signal: %s\n", signal.toString());
            p.interrupt();
        }
    }
}
