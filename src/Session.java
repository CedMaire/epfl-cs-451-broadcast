import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketException;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.HashMap;


/**
* Session class and methods
*
* @author Mathilde Raynal, Cedric Maire, Benjamin Deleze
*/

class Session {
  private final HashMap<Integer, Node> nodes;
  private final int self;
  private final Set<Message> delivered;
  private final Set<Message> messagesToBroadcast;
  private final int numberOfNodes;
  private final Set<Message> deliveredDone;
  private final Set<Message> waitingForDelivery;

  private boolean started = false;
  private boolean stopped = false;

  private long startTime = 0;
  private long stopTime = 0;

  private DatagramSocket ds;

  Session(HashMap<Integer, Node> nodes, int self) {
    this.self = self;
    this.nodes = nodes;
    ConcurrentHashMap<Message, Integer> mapForsetD = new ConcurrentHashMap<>();
    this.delivered = mapForsetD.keySet(0);
    ConcurrentHashMap<Message, Integer> mapForset = new ConcurrentHashMap<>();
    this.messagesToBroadcast = mapForset.keySet(0);
    ConcurrentHashMap<Message, Integer> mapForsetW = new ConcurrentHashMap<>();
    this.waitingForDelivery = mapForsetW.keySet(0);
    this.numberOfNodes = nodes.size();

    ConcurrentHashMap<Message, Integer> mapForsetF = new ConcurrentHashMap<>();
    this.deliveredDone = mapForsetF.keySet(0);

    try {
      ds = new DatagramSocket(Objects.requireNonNull(nodes.get(self)).getIP());
      ds.setReuseAddress(true);
    } catch (SocketException e) {
      e.printStackTrace();
      System.out.println("ERROR: datagram socket creation failed");
      System.exit(1);
    }
  }

  /**
  * Print the present state of the process
  */
  private void printSate() {
    System.out.println("\n=============================");
    System.out.println("* Nodes:\n" + nodes);
    System.out.println("* Broadcast logs:\n" + Logger.getBroadcasts());
    System.out.println("* Delivery logs:\n" + Logger.getDeliveries());
    System.out.println("* Delivered:\n" + delivered);
    System.out.println("* To send:\n" + messagesToBroadcast);
    System.out.println("=============================\n");
  }

  /**
  * Created messages based on content and adds them in the set of messages to be sent.
 * @throws InterruptedException
  */
  /*
  private void fillMyMessages(int n) {
    for (int i = 1; i <= n; ++i) {
      messagesToBroadcast.add(new Message(i, self, false));
    }
  }
  */

  /**
   * Create the new message with its dependencies
   * 
   * @param i, the id of the message
   */
  private void createMessage(int i){
    synchronized (this) {
      Message msg = new Message(i, self, false, new ArrayList<>());
      for(Integer dep : Setup.getDependencies()) {
        int last = nodes.get(dep).getLastMessage();
        if(last > 0) {
          msg.addDependencies(new PairI(dep, last));
        }
      }
      this.messagesToBroadcast.add(msg);

      Logger.broadcast(msg);
    }
  }

  /**
  * Span thread used to send messages.
 * @throws InterruptedException
  */

  private void startSending() {
    boolean finish = false;
    int round = 1;
    createMessage(round);
    round++;

    startTime = System.nanoTime();
    while (true) {
      if(round <= Setup.getNumberMessagesToBroadcast()) {
        // Thread.sleep((long)(Math.random() * 1000));
        createMessage(round);
        round++;
      }
      Set<Message> toSend = new HashSet<>(messagesToBroadcast);
      if(finish) {
    	  // System.out.println("Finish");
      }
      for (Message message : toSend) {
        if (canSend(message, round)) {
          broadcast(message);
        }
      }
      finish = messagesToBroadcast.isEmpty();
    }
  }

  /**
  * Span thread used to receive messages.
  */
  private void startListening() {
    while (true) {
      byte[] buf = new byte[1024];
      DatagramPacket dp = new DatagramPacket(buf, 1024);

      Packet packet = receive(dp);
      handle(packet.getValue(), packet.getKey());
    }
  }


  /**
  * Starts the communication with the other nodes
  */
  void start() {
    if (!started) {

      //this.fillMyMessages(Setup.getNumberMessagesToBroadcast());

      Setup.addThread(new Thread(this::startListening));
      Setup.addThread(new Thread(this::startSending));

      for (Thread thread : Setup.getThreadsToKill()) {
        thread.start();
      }

      started = true;
    }
  }

  /**
  * Stops the session by killing active threads and stuff.
  */
  void stop() {

    stopped = true;
    for (Thread thread : Setup.getThreadsToKill()) {
      thread.interrupt();
    }

    ds.close();
    Logger.write();
    System.exit(0);
  }

  /**
  * Sends the message to the destination.
  *
  * @param msg,  the message to be sent
  * @param dest, the node to send to
  */
  private void send(Message msg, Node dest) {
    try {
      byte[] buffer = msg.marshal();
      DatagramPacket packet = new DatagramPacket(buffer, buffer.length, dest.getIP());
      ds.send(packet);
    } catch (Exception e) {
      if (!stopped) {
        e.printStackTrace();
        System.out.println("ERROR: send failed");
      }
    }
  }

  /**
  * Receives packet and unmarshals it
  *
  * @param dp, the packet tp receive
  */
  private Packet receive(DatagramPacket dp) {
    try {
      ds.receive(dp);
    } catch (IOException e) {
      if (!stopped) {
        e.printStackTrace();
        System.out.println("ERROR: receive on datagram socket failed");
      }
      return new Packet(null, null);
    }

    for (Node node : nodes.values()) {
      if (node.getIP().toString().equals(dp.getSocketAddress().toString())) {
        return new Packet(node, Message.unmarshal(Arrays.copyOfRange(dp.getData(), 0, dp.getLength())));
      }
    }
    System.out.println("ERROR: origin node not found");
    return new Packet(null, null);
  }

  /**
  * Broadcasts the message to the list of nodes of the session.
  *
  * @param msg, the message to be broadcasted
  */
  private void broadcast(Message msg) {
    // add our own ack to ack table (we use creatorID and not self because we may be forwarding a msg)
    Objects.requireNonNull(nodes.get(msg.getCreatorID())).addAck(msg.getContent(), self);

    int received = 0;
    for (Node n : nodes.values()) {
      // n did not ack this msg, we send it again
      if (!Objects.requireNonNull(nodes.get(msg.getCreatorID())).hasAckFrom(n.getId(), msg.getContent())) {
        // System.out.println("Broadcast to " + n.getIP().toString());
        send(msg, n);
      } else {
        received++;
      }
    }

    //Logger.broadcast(msg);

    // we are done broadcasting this message to everyone
    if (received == numberOfNodes) {
      messagesToBroadcast.remove(msg);
    }
  }

  /**
  * Handles the message according to its type and update the nodes' information.
  *
  * @param msg, the message to be broadcasted
  * @param src, the node we received the msg from
  */
  private void handle(Message msg, Node src) {

    if (msg == null) {
      return;
    }

    if (msg.isAck()) {
      // update ack table
      Objects.requireNonNull(nodes.get(msg.getCreatorID())).addAck(msg.getContent(), src.getId());
      // do we move on to sending our next message if this is the last ack we expect ?
      // if enough nodes acked it, we can deliver and wait for the next one
    } else {
      // (1) update forward set (actually may not need forward set)
      // (2) add ack for src for this message from creator id
      // and also we assume creatorID acked his own message
      // our own ack will be added in (4)
      Objects.requireNonNull(nodes.get(msg.getCreatorID())).addAck(msg.getContent(), src.getId());
      Objects.requireNonNull(nodes.get(msg.getCreatorID())).addAck(msg.getContent(), msg.getCreatorID());
      Objects.requireNonNull(nodes.get(msg.getCreatorID())).addAck(msg.getContent(), self);
      waitingForDelivery.add(msg);
      // (4) send Ack to src (no loop, if did not reach, message will be resent to us)
      send(new Message(msg.getContent(), msg.getCreatorID(), true, new ArrayList<>(msg.getD())), src);
      // (5) add message to messages to be broadcasted (in (2) we pretend that src acked it so they won't receive it)
      // also we won't send it back to creatorID because we assume they ack it
      messagesToBroadcast.add(msg);
    }
    tryToDeliver(msg, false);
  }

  /**
  * Check if the message is the next to be delivered
  *
  * @param msg, the message we want to check
  * @return true if it is the case, false otherwise
  */
  private boolean isLast(Message msg) {
    return Objects.requireNonNull(nodes.get(msg.getCreatorID())).getLastMessage() == (msg.getContent() - 1);
  }

  /**
  * Check if the message is the next to be send
  *
  * @param msg, the message we want to check
  * @return true if it is the case, false otherwise
  */
  private boolean canSend(Message msg, int round) {
    return Objects.requireNonNull(nodes.get(msg.getCreatorID())).getLastMessage() >= (msg.getContent() - 1) || round >= msg.getContent();
  }

  /**
  * Check if the message was received by the majority of the nodes
  *
  * @param msg, the message we want to check
  * @return true if it is the case, false otherwise
  */
  private boolean hasMajority(Message msg) {
    return Objects.requireNonNull(nodes.get(msg.getCreatorID())).numberOfAck(msg.getContent()) > numberOfNodes / 2;
  }

  /**
   * Check if we have already deliver all messages that "caused" this message
   *
   * @param msg, the message we want to check
   * @return true if it is the case, false otherwise
   */
  private boolean isDependenciesOk(Message msg) {
	  boolean ok = true;
	  for(PairI p : msg.getD()) {
		  if(Objects.requireNonNull(nodes.get(p.getKey())).getLastMessage() < p.getValue()) {
			  ok = false;
			  break;
		  }
	  }
	  return ok;
  }
  /**
  * Increase the last message delivered number of a node
  *
  * @param msg, the message, to get the node we want to increase
  */
  private void increaseLast(Message msg) {
    Objects.requireNonNull(nodes.get(msg.getCreatorID())).updateLastMessage();
  }

  /**
  * Try to deliver a given message
  *
  * @param msg, the message we want to deliver
  */
  private void tryToDeliver(Message msg, boolean recurse) {
    if (isLast(msg) && hasMajority(msg) && isDependenciesOk(msg)) {
      deliver(msg);
      delivered.add(msg);
      increaseLast(msg);
      waitingForDelivery.remove(msg);
    }

    if (!recurse) {
      for(Message m : waitingForDelivery) {
        tryToDeliver(m, true);
      }
    }
  }

  /**
  * Delivers is executed when enough nodes got this message. It calls Logger
  *
  * @param msg, the message to be delivered
  */
  private void deliver(Message msg) {
    // System.out.println("Deliver: " + msg);
    synchronized (this) {
      Logger.delivery(msg);
      deliveredDone.add(msg);
      /*
      if (msg.getContent() == Setup.getNumberMessagesToBroadcast()) {
          stopTime = System.nanoTime();
          System.out.println(deliveredDone.toString());
          System.out.println("!!! DONE !!! (" + (double) (stopTime - startTime) / 1_000_000_000.0 + " seconds)");
        }
       */
    }
  }
}

class Packet {
  private final Node key;
  private final Message value;

  Packet(Node node, Message message) {
    key = node;
    value = message;
  }

  Node getKey() {
    return key;
  }

  Message getValue() {
    return value;
  }

  @Override
  public String toString() {
    return "(" + key + ", " + value + ")";
  }
}
