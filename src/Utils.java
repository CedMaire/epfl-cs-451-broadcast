import java.util.ArrayList;

class Message {
    private final int content;
    private final int creatorID;
    private final boolean isAck;
    private final ArrayList<PairI> dependencies;

    public Message(int content, int creatorID, boolean isAck, ArrayList<PairI> dependencies) {
        this.content = content;
        this.creatorID = creatorID;
        this.isAck = isAck;
        this.dependencies = new ArrayList<PairI>(dependencies);
    }

    /**
     * A method to unmarshal an byte buffer
     *
     * @param buffer, the buffer with the message inside
     * @return the message in a Message format
     */
    static Message unmarshal(byte[] buffer) {
        String string = new String(buffer);
        String[] parts = string.split(";");
        int content = Integer.parseInt(parts[0]);
        int creatorID = Integer.parseInt(parts[1]);
        int ackInt = Integer.parseInt(parts[2]);
        ArrayList<PairI> dependencies = new ArrayList<>();
        if(parts.length > 3) {
        	for(int i = 3; i < parts.length; i=i+2) {
        		PairI p = new PairI(Integer.parseInt(parts[i]),Integer.parseInt(parts[i+1]));
        		dependencies.add(p);
        	}
        }
        boolean isAck = false;
        if (ackInt == 1) {
            isAck = true;
        }
        return new Message(content, creatorID, isAck, dependencies);
    }

    /**
     * A getter for the content
     *
     * @return content
     */
    int getContent() {
        return content;
    }

    /**
     * A getter for the creatorID
     *
     * @return creatorID
     */
    int getCreatorID() {
        return creatorID;
    }

    /**
     * Check if the message is an Ack
     *
     * @return true if it is the case, false otherwise
     */
    boolean isAck() {
        return isAck;
    }

    /**
     * Add a new dependencie to the dependencies
     * 
     * @param p the new pair
     */
    void addDependencies(PairI p) {
    	this.dependencies.add(p);
    }
    
    /**
     * A getter for the dependencies
     * 
     * @return a copy of the dependencies
     */
    ArrayList<PairI> getD() {
    	return new ArrayList<PairI>(this.dependencies);
    }
    
    @Override
    public boolean equals(Object o) {
        if (!(o instanceof Message)) return false;
        Message mo = (Message) o;
        return this.content == mo.getContent() && this.creatorID == mo.getCreatorID();
    }

    @Override
    public int hashCode() {
        return Integer.parseInt(this.creatorID + "" + this.content);
    }

    /**
     * Put the message in the right format to be send
     *
     * @return the message as a byte array
     */
    byte[] marshal() {
        String ackStr = "0";
        if (this.isAck) {
            ackStr = "1";
        }
        String str = this.content + ";" + this.creatorID + ";" + ackStr;
        for (PairI p : this.dependencies) {
        	str += ";" + p.getKey() + ";" + p.getValue();
        }
        return str.getBytes();
    }

    @Override
    public String toString() {
        String type = "Message: ";
        if (this.isAck) {
            type = "Ack: ";
        }
        return type + "(content: " +
                this.content +
                ", creator: " +
                this.creatorID +
                ")";
    }
}

/**
 * A helper class representing a Pair of int
 */
class PairI {
  private final int key;
  private final int value;

  PairI(int l, int s) {
    key = l;
    value = s;
  }

  int getKey() {
    return key;
  }

  int getValue() {
    return value;
  }

  @Override
  public String toString() {
    return "(" + key + ", " + value + ")";
  }

  @Override
  public boolean equals(Object o) {
      if (!(o instanceof PairI))
          return false;
      PairI no = (PairI) o;
      return this.value == no.value && this.key == no.key;
  }
}
