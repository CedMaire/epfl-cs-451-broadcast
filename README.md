# da2019

`make` to compile. `java Da_proc i membership m` will start node with id i, membership file membership and number of messages m. As we have 5 nodes, we need 5 terminals.
In a 6th: `ps aux | grep Da_proc | awk {'print $2'} | xargs kill -s USR2` will start all sessions. `ps aux | grep Da_proc | awk {'print $2'} | xargs kill -s INT` will stop them all
