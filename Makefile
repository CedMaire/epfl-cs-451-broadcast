all: main

# validate.sh executes make to obtain the executable da_proc (or Da_proc.class).

main:
	mkdir -p bin
	javac -d bin/ src/*.java 2> /dev/null
	cp -r bin/* .

clean:
	rm -f bin/*.class
	rm -f *.class
	rm -f *.out
	rm -f *.java
	rm -f src/*.class
