# make id=X
# ps aux | grep ch.epfl.da.Main | awk {'print $2'} | xargs kill -s USR2
# ps aux | grep ch.epfl.da.Main | awk {'print $2'} | xargs kill -s INT

make clean
make

echo "10
1 127.0.0.1 11001
2 127.0.0.1 11002
3 127.0.0.1 11003
4 127.0.0.1 11004
5 127.0.0.1 11005
6 127.0.0.1 11006
7 127.0.0.1 11007
8 127.0.0.1 11008
9 127.0.0.1 11009
10 127.0.0.1 11010" > membership

echo Spawning...

java Da_proc 1 membership 1000 &
java Da_proc 2 membership 1000 &
java Da_proc 3 membership 1000 &
java Da_proc 4 membership 1000 &
java Da_proc 5 membership 1000 &
java Da_proc 6 membership 1000 &
java Da_proc 7 membership 1000 &
java Da_proc 8 membership 1000 &
java Da_proc 9 membership 1000 &
java Da_proc 10 membership 1000 &

sleep 5

ps aux | grep "java Da_proc" | awk {'print $2'} | xargs kill -s USR2

echo Starting...

sleep 600
